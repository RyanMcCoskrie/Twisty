require 'twisty'

engine = Twisty::Engine.instance()

startRoom = engine.define_room(:start, "Start", "The beginning of the game")
middleRoom1 = engine.define_room(:middle1, "Middle", 
	"A passage full of twisty little mazes")
middleRoom2 = middleRoom1.clone(:middle2)
endRoom = engine.define_room(:end, "End", "The end of the game")

engine.add_door(:start, :middle1, "Forward")
engine.add_door(:middle1, :start, "Back")
engine.add_door(:middle1, :middle2, "Forward")
engine.add_door(:middle2, :middle1, "Back")
engine.add_door(:middle2, :end, "Down")

tree1 = engine.define_item(:tree1, "Tree", "A big leafy tree", :fixed => true)
tree2 = tree1.clone(:tree2)
sword = engine.define_item(:sword, "Sword", "Shiny and pointy")
stone = engine.define_item(:stone, "Stone",
	"A grey rock with the hilt of a sword sticking out", :fixed => true,
	:storage => 1)
apple = engine.define_item(:apple, "Apple", "A shiny red apple")
chest = engine.define_item(:chest, "Chest", "A big wooden box", :fixed => true,
	:storage => 1)

stone.on_take_content do |item|
	puts "As you draw the sword you wonder if a wizard put it there"
	return true
end

stone.on_put_content do |item|
	if item == sword.id
		return true
	else
		puts "That does not fit in there"
		return false
	end
end

sword.on_take do
	puts "Drawing the sword you realise it is exactly the treasure you wanted"
	puts

	return true
end

sword.on_drop do
	puts "Now that you have the sword you can never abondon it"
	puts

	return false
end

startRoom.add_item(:tree1)
middleRoom1.add_item(:stone)
middleRoom1.add_item(:chest)
middleRoom2.add_item(:tree2)
stone.add_item(:sword)
engine.give(:apple)

startRoom.on_exit do
	unless engine.inventory.include? sword.id
		puts "You decide to search for treasure"
		puts
	end

	return true
end

endRoom.on_enter do
	if engine.inventory.include? sword.id
		puts "You have won the game!"
		puts
		engine.end_loop()
		return true
	else
		puts "You remember you haven't found the treasure and turn around"
		return false
	end
end

engine.define_command(
	/^swing$/,
	"swing",
	"swing the sword you are carrying") do |x|
		if engine.inventory.include? sword.id
			puts "Swing! Swish! Clang!"
		else
			puts "You are not carrying a sword"
		end
	end

engine.start_loop()
