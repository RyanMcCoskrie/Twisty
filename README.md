Twisty
======

A CLI Adventure Game Engine
---------------------------

Twisty is an Ruby library for creating command line games in the tradition of
the Collosal Cave Adventure.

The key design considerations of Twisty are simplicity and extensibility.

* [Features](#features)
* [Requirements](#requirements)
* [Install](#install)
* [Usage](#usage)
	* [Structure](#structure) The sections of a typical Twisty game
* [Built in commands](#built-in-commands)

## Features

* Easy to install, code and deploy
* Events on entering and exiting Rooms
* Events on picking up and dropping Items
* Support for Custom commands
* Support for non-linear environments (i.e. One way doors)

## Requirements
- Ruby >= 2.4.0
- rtype >= 0.6.8
- rtype-native >= 0.6.8

## Install
At present the API of Twisty is too unstable for widespread use but if you
wish to experiment with it please use:

```sh
$ gem install twisty
```
## Usage
This is only intended as a introduction. Twisty's source code includes extensive
annotations exported as HTML to the doc/ directory of the source tree.

It is assumed that you are at least a little familiar with the Ruby programming
language.

**NOTE** All examples in the following are taken from the file test.rb

## Classes

### Engine
This class is a singleton that encapsulates all data within a game written in
Twisty and provides the API that the other three classes are accessed through.

### Room
Each instance of Room is used to detail one location in the game.

Keep in mind that all Rooms are stored by a private variable of the
Engine class. Creating new Rooms is detailed in
[Room definitions](#room-definitions). The array of all Rooms can be accessed
through Twisty::Engine::rooms() and the current Room can be accessed through
Twisty::Engine::current_room()

### Item
Item is used to model the objects that the player can interact with.

### Structure
The major sections of Twisty program are:
* [Setup](#setup)
* [Room definitions](#room-definitions)
* [Door definitions](#door-additions)
* [Item definitions](#item-definitions)
* [Item additions](#item-additions)
* [Room events](#room-events)
* [Item events](#item-events)
* [Custom commands](#custom-commands)
* [Loop invocation](#loop-invocation)

### Setup
This section is by far the simplist. Techincally it only needs to include

```ruby
require 'twisty'
```
However it is strongly advised, for the sake of brevity, to follow it with a
line such as:
```ruby
engine = Twisty::Engine.instance()
```
All examples below assume that this has been included in the Setup section.

### Room definitions
This section the locations in game a defined using the method
Twisty::Engine::define_room().

Example:
```ruby
	engine.define_room(:start, "Start", "The beginning of the game")
	engine.define_room(:middle, "Middle", "A passage full of twisty little mazes")
	engine.define_room(:end, "End", "The end of the game")
```
Note that the Symbol for each Room must be unique while the name and description
do not.

### Door additions
A door is used to indicate the posibility of the player moving from one Room
to another and is created using Twisty::Engine::add_door().

Note that a door only works in one direction and that the name must be a single
word but does not have to be descriptive of the destination.

Example:
```ruby
	engine.add_door(:start, :middle, "Forward")
	engine.add_door(:middle, :start, "Back")
	engine.add_door(:middle, :end, "Down")
```

### Item definitions
Here the Items that are contained in the Rooms are detailed using
Twisty::Engine::define_item().

Examples:
```ruby
	engine.define_item(:sword, "sword", "Shiny and pointy")
	engine.define_item(:stone, "stone", "A grey rock with the hilt of a sword sticking out",
		:fixed => true, :storage => 1)
```

### Item additions
Once an Item has been created it must be given a location. For this there are
three options:

* Inside a Room
* Inside another Item
* In the player's inventory

The respective methods for these are Twisty::Room::add_item(),
Twisty::Item::add_item() and Twisty::Engine::give. Each takes the Symbol of an
Item as its argument.

Note that Item and Room objects are accessed through the convinience functions
Twisty::Engine::items and Twisty::Engine::rooms.

Examples:
```ruby
	engine.rooms[:start].add_item(:tree)
```
*Places the Item :tree in the Room :start*

```ruby
	engine.items[:stone].add_item(:sword)
```
*Places the Item :sword inside the Item :stone*

```ruby
	engine.give(:apple)
```
*Places the Item :apple in the player's inventory*

### Room events
In this section blocks of code are listed for the events that occur when
entering or exiting a distinct Room. Note that the code must return a Boolean
to indicate if the player can successfully move forward.

The two relevant methods are Engine::Twisty::on_enter() and
Engine::Twisty::on_exit().

Examples:
```ruby
	engine.rooms[:start].on_exit do
		unless engine.inventory.include? :sword
			puts "You decide to search for treasure"
			puts
		end

		return true
	end
```
*Displays a message on leaving the :start Room if the player is not carrying
the :sword Item.*

```ruby
	engine.rooms[:end].on_enter do
		if engine.inventory.include? :sword
			puts "You have won the game!"
			puts
			engine.end_loop()
			return true
		else
			puts "You remember you haven't found the treasure and turn around"
			return false
		end
	end
```
*Prevents the player from entering the :end Room if not carrying the :sword
Item.*

### Item events
These are the same in principle as [Room events](#room-events) but control
whether the player can pick up or drop the relevant object and what happens
should the player do so. The methods for these are Twisty::Engine::on_take(),
Twisty::Engine::on_drop(), Twisty::Engine::on_put_content() and
Twisty::Engine::on_take_content().

Examples:
```ruby
	engine.items[:sword].on_drop do
		puts "Now that you have the sword you can never abondon it"
		puts

		return false
	end
```
*Prevents the player from dropping the sword*

### Custom commands
Here game specific commands are implemented using the
Twisty::Engine::define_command() method. Each command requires a unique Regular
expression, an example, a short description and a block of code.

Example:
```ruby
	engine.define_command(
		/^swing$/,
		"swing",
		"swing the sword you are carrying") do
			if inventory.include? :sword
				puts "Swing! Swish! Clang!"
			else
				puts "You are not carrying a sword"
			end
		end
```
*Writes "Swing! Swish! Clang!" if the user types "swing" while carrying the
:sword Item.*

### Loop invocation
Finally the game main loop of the game must be invoked using
Twisty::Engine::start_loop(). Note that the developer is free to include
introductory events before this.

## Built in commands

The following is a list of commands that are available to players in any game
produced with Twisty.

* **help** Writes a list of all commands (including game specific ones) with
	help messages.
* **look** Look at the current Room and list it's contents
* **look at Item** Look at a specific Item
* **inventory** List all Items the player is carrying
* **take Item** Remove an Item from the current Room and place in inventory
* **take Item from Item** Remove one item from inside of another and place in
	inventory
* **drop Item** Remove an Item from the inventory and leave it in the current
	room
* **put Item in Item** Remove an Item from the inventory and place it inside
	another Item
* **walk Exit** Leave the current room and enter another
* **quit** Quit playing the game
