#############################################################################
#                                                                           #
#   Copyright (c) 2017, Ryan McCoskrie <work@ryanmccoskrie.me>              #
#                                                                           #
#   This software is distributed under the MIT License which can be found   #
#   in the root directory.                                                  #
#                                                                           #
#############################################################################

require 'rtype'

require_relative 'error.rb'

#Author:: Ryan McCoskrie (mailto:work@ryanmccoskrie.me)
#Copyright:: Copyright (c) 2017 - 2018 RyanMcCoskrie
#License:: MIT public license

module Twisty

#Super-class of Room and Item. Do not use directly.
#
#*NOTE* It is possible this class may be removed in future versions.
class Entity
	#(+Symbol+)The Symbol used to distinguish the Entity from other memebers
	#of the same sub-class
	attr_reader :id

	rtype [Entity] => Boolean
	#(+Boolean+)
	#
	#Checks if two different instances of Entity refer to the same datum
	#
	#other:: (+Entity+) instance of Entity to check for equality against
	def ==(other)
		if other.class == self.class
			return other.id == @id
		elsif other.class == Symbol
			return other == @id
		else
			return false
		end
	end

	rtype [Symbol] => nil
	#Creates an identical instance of the Entity
	#
	#other:: (+Symbol+) Index entry of the new instance
	def clone(other)
		raise GameError.new "Can not use Entity directly. Please use subclass instead"
		return nil
	end

	#(+Engine+)
	#
	#Convinience class to access the Engine
	def engine
		return Engine.instance()
	end
end

end
