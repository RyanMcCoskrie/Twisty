#############################################################################
#                                                                           #
#   Copyright (c) 2017, Ryan McCoskrie <work@ryanmccoskrie.me>              #
#                                                                           #
#   This software is distributed under the MIT License which can be found   #
#   in the root directory.                                                  #
#                                                                           #
#############################################################################

require_relative 'engine.rb'

require 'rtype'

module Twisty

#Used to provide the descriptions, examples of and executable code of commands
#that the player can type.
class Command
	#(+String+) An example of the command e.g. "take item"
	attr_reader :help1
	#(+String+) Short description of what the command actually does
	attr_reader :help2

	#Initialiser. Use Engine.define_command() instead
	#
	#help1:: (+String+) An example of the command e.g. "take item"
	#help2:: (+String+) Short description of what the command actually does
	#func:: (Proc) Code that provides the commands effect(s). *NOTE* This
	#       must take exactly one argument of type +Array+[+String+]
	def initialize(help1, help2, func)
		@help1 = help1
		@help2 = help2

		self.define_singleton_method(:exec, func)
	end

	#Executes this instance of command. This is redefined on a per-instance
	#basis by Command.new().
	#
	#args:: (+Array+[+String+]) Words the player typed in
	def exec(args)
		return
	end

	protected
	#(+Engine+)
	#
	#Convinience class so that each instances version of Command.exec() does
	#not need to contain
	#    engine = Engine.instance
	def engine
		Twisty::Engine.instance()
	end
end

end
