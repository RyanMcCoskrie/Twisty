#############################################################################
#                                                                           #
#   Copyright (c) 2017, Ryan McCoskrie <work@ryanmccoskrie.me>              #
#                                                                           #
#   This software is distributed under the MIT License which can be found   #
#   in the root directory.                                                  #
#                                                                           #
#############################################################################

module Twisty

#Super-class of GameError and PlayError. Do not use directly
class TwistyError < RuntimeError
	#(+String+) Message detailing the exception
	attr_reader :message

	#Initialiser
	#
	#s:: (+String+) Contents of TwistyError.message
	def initialize(s)
		@message = s
	end
end

#Used when Twisty has found a bug in a specific game
class GameError < TwistyError
end

#Used to tell the player when (s)he has used a command incorrectly
class PlayError < TwistyError
end

end
