#############################################################################
#                                                                           #
#   Copyright (c) 2017, Ryan McCoskrie <work@ryanmccoskrie.me>              #
#                                                                           #
#   This software is distributed under the MIT License which can be found   #
#   in the root directory.                                                  #
#                                                                           #
#############################################################################

require_relative 'entity'

require 'rtype'


#Author:: Ryan McCoskrie (mailto:work@ryanmccoskrie.me)
#Copyright:: Copyright (c) 2017 - 2018 RyanMcCoskrie
#License:: MIT public license

module Twisty

# This class models objects that (movable and otherwise) that the character
# can interact with.
class Item < Entity
	#(+String+) One-word name of the Item
	attr_reader :name
	#(+String+) Description printed when the Item is looked at.
	attr_reader :desc
	#(+Boolean+) Indicates if the Item can not be moved
	attr_reader :fixed
	#(+Integer+) Number of other Items this instance can store
	attr_reader :storage
	#(+Array+[+Symbol+]) Other Item this instance is currently storiing
	attr_reader :contents

	#Initialiser. Please use Engine.define_item() instead
	#
	#id:: (+Symbol+) Key in Engine.items of the Item
	#name:: (+String+) One-word name of the Item
	#desc:: A long description of the Item printed when the player uses
	#       "look at item"
	#fixed:: (+Boolean+) If set to +false+ the Item can not be taken
	#storage:: (+Integer+) The number of other Items this instance can hold
	def initialize(id, name, desc, fixed, storage)
		@id = id
		@name = name
		@desc = desc
		@fixed = fixed
		@storage = storage
		@contents = []
	end

	rtype [Symbol] => Item
	#(+Item+)
	#
	#Creates another Item identical to this instance
	#
	#other:: (+Symbol+) Index of the new instance of Item
	def clone(other)
		return engine.define_item(other, @name, @desc,
			:fixed => @fixed, :storage => @storage)
	end
	rtype [] => Boolean
	#(+Boolean+)
	#
	#Executed when the Item is picked up. This function can be redefined
	#on a per-instance basis using on_take(&block). If this returns false
	#take will fail
	def take_event
		return true
	end

	rtype [] => Boolean
	#(+Boolean+)
	#
	#Executed when the Item is picked up. This function can be redefined
	#on a per-instance basis using on_drop(&block). If this returns false
	#drop will fail.
	def drop_event
		return true
	end

	rtype [Symbol] => Boolean
	#(+Boolean+)
	#
	#Executed when another Item is placed inside this instance. This
	#function can be redefined on a per-instance basis using on_add(&block).
	#If this return false add_item will fail
	#
	#item:: (+Symbol+) Key in Engine.items of the item being added
	def put_content_event(item)
		return true
	end

	rtype [Symbol] => Boolean
	#(+Boolean+)
	#
	#Executed an Item inside this instance of Item is removed by the player.
	#If this returns false "take X from Y" will fail.
	#
	#item:: (+Symbol+) Key in Engine.items of the item being taken
	def take_content_event(item)
		return true
	end

	rtype [] => nil
	#Prints the description and the names of the contents if applicable
	def look
		puts @desc

		if @storage > 0
			puts "Contents:"

			if @contents.length == 0
				puts "Nothing"
			else
				@contents.each{|id| puts engine.items[id].name}
			end
		end

		return nil
	end

	rtype [Symbol] => Boolean
	#(+Boolean+)
	#
	#Returns +true+ if this item contains another represented by the symbol
	#item
	def contains_item?(item)
		return @contents.include?(item) if @storage > 0
	end

	rtype [Symbol] => nil
	#Stores another Item represented by the Symbol item and stores it
	#within this Item if:
	# * storage > 0
	# * contents.size < 0
	# * insert_event() returns true
	def add_item(item)
		if @storage == 0
			raise GameError.new "#{@id} is not a container"
		else
			#Skip if already contained
			if contains_item?(item) == false
				@contents << item
			end
		end

		return nil
	end

	rtype [Symbol] => nil
	#Removes an Item contained within this instance
	#*NOTE* This does not place the Item in the player's inventory
	#
	#item:: (+Symbol+) Key in Engine.items of the Item being placed in this
	#       instance of Item
	def del_item(item)
		#Skip if already contained
		if contains_item?(item)
			@contents.delete item
		end

		return nil
	end

	rtype [Proc] => nil
	#Redefines Item::take_event for this specific instance
	#
	#&block:: (+Proc+ => +Boolean+) The new version of Item.take_event for
	#         this specific instance *NOTE* Must return a +Boolean+
	def on_take(&block)
		self.define_singleton_method(:take_event, &block)
		return nil
	end

	rtype [Proc] => nil
	#Redefines Item::drop_event for this specific instance
	#
	#&block:: (+Proc+ => +Boolean+) The new version of Item.drop_event for
	#         this specific instance *NOTE* Must return a +Boolean+
	def on_drop(&block)
		self.define_singleton_method(:drop_event, &block)
		return nil
	end

	rtype [Proc] => nil
	#Redefines Item::put_content_event for this specific instance
	#
	#&block:: (+Proc+ => +Boolean+) The new version of
	#         Item.put_content_event for this specific instance.
	#         *NOTE* Must return a +Boolean+
	def on_put_content(&block)
		self.define_singleton_method(:put_content_event, &block)
		return nil
	end

	rtype [Proc] => nil
	#Redefines Item::take_content_event for this specific instance
	#
	#&block:: (+Proc+ => +Boolean+) The new version of
	#         Item.take_content_event for this specific instance.
	#         *NOTE* Must return a +Boolean+
	def on_take_content(&block)
		self.define_singleton_method(:take_content_event, &block)
		return nil
	end
end

end
