#############################################################################
#                                                                           #
#   Copyright (c) 2017, Ryan McCoskrie <work@ryanmccoskrie.me>              #
#                                                                           #
#   This software is distributed under the MIT License which can be found   #
#   in the root directory.                                                  #
#                                                                           #
#############################################################################

require_relative 'entity.rb'

require 'rtype'

#Author:: Ryan McCoskrie (mailto:work@ryanmccoskrie.me)
#Copyright:: Copyright (c) 2017 - 2018 RyanMcCoskrie
#License:: MIT public license

module Twisty

#This class models the locations within the game.
class Room < Entity
	#(+String+) Short name shown in the prompt
	attr_reader :name
	#(+String+) Description printed when the player types "look"
	attr_reader :desc
	#(+Hash+{+String+ => +Symbol+}) Names and keys in Engine.rooms of the
	#locations the player can move from
	attr_reader :doors
	#(+Array+[+String+]) Items in the room. Does not include Items stored
	#inside other Items
	attr_accessor :items

	rtype [Symbol, String, String] => Array
	#Initializer. Please use Twisty::Engine::define_room() instead.
	#
	#id:: (+Symbol+) that represents the Room in Engine.rooms()
	#name:: (+String+) Name shown to the player in the prompt
	#desc:: (+String+) Long description shown when the player types "look"
	def initialize(id, name, desc)
		@id = id
		@name = name
		@desc = desc
		@doors = {}
		@items = []
	end

	rtype [Symbol] => Room
	#(+Room+)
	#
	#Creates another Item identical to this instance
	#
	#other:: (+Symbol+) Index of the new instance of Item
	def clone(other)
		return engine.define_room(other, @name, @desc)
	end

	rtype [] => Boolean
	#(+Boolean+)
	#
	#Executed when the player attempts to enter the Room. This can be
	#redefined on a per-instance basis using Room.on_enter(&block). If this
	#returns +false+ the player will be blocked from entering
	def enter_event
		return true
	end

	rtype [] => Boolean
	#(+Boolean+)
	#
	#Executed when the player attempts to exit the Room. This can be
	#redefined on a per-instance basis using on_exit(&block). If this
	#returns +false+ the player will be blocked from exiting
	def exit_event
		return true
	end

	rtype [Symbol, String] => nil
	#Adds an exit from this room to another
	#*NOTE* For most purposes Engine.add_door() would be more convinient
	#*NOTE* The "door" only works in one direction and that the exit is
	#referred to by the String name, not the name of the other Room.
	#
	#to:: (+Symbol+) Key in Engine.rooms of the location the player will
	#     be moved to if "walk" is sucessfully executed
	#name:: (+String+) Name displayed by "look" and typed as part of "walk".
	#       This can be a word such as "East" instead of the destination's
	#       actual name
	def add_door(to, name)
		if name.split().length != 1
			raise GameError.new "Door name must be one word"
		elsif @doors.has_key? name
			raise GameError.new "Door with name #{name} already exist in #{@id}"
		else
			@doors[to] = name
		end

		return nil
	end

	rtype [Symbol] => Array.of(Symbol)
	#Places an Item within a the Room. Note that is must first be defined
	#with Twisty::Engine::define_item()
	#
	#id:: (+Symbol+) Key in Engine.items for the Item that will be added to
	#     the Room
	def add_item(id)
		if @items.include? id
			raise GameError.new "Duplicate item in room #{@id}"
		else
			if $engine.items.include? id
				@items << id
			else
				raise GameError.new "Attempt to add invalid item #{id} to room #{@id}"
			end
		end
	end

	rtype [Proc] => nil
	#Used to redefine enter_event
	#
	#&block:: (+Proc+ => +Boolean+) Code executed on entering the Room.
	#         *NOTE* If this returns +false+ the player "walk" will fail
	def on_enter(&block)
		self.define_singleton_method(:enter_event, &block)
		return nil
	end

	rtype [Proc] => nil
	#Used to redefine exit_event
	#
	#&block:: (+Proc+ => +Boolean+) Code executed on exiting the Room.
	#         *NOTE* If this returns +false+ the player "walk" will fail
	def on_exit(&block)
		self.define_singleton_method(:exit_event, &block)
		return nil
	end

	rtype [] => nil
	#Prints the description of the Room and its contents / exits if
	#applicable.
	def look
		puts @desc

		if @items.length == 0
			puts "Nothing is located here"
		elsif @items.length == 1
			puts

			puts "Located here is:"
			puts engine.items[@items[0]].name
		else
			puts

			puts "Located here are:"
			@items.each{ |id| puts engine.items[id].name }
		end

			puts

		if doors.length == 0
			puts "There is no exit"
		elsif doors.length == 1
			puts "The only exit is: #{@doors.values[0]}"
		else
			puts "The exits are:"
			@doors.values.each{|name| puts name}
		end

		return nil
	end
end

end
