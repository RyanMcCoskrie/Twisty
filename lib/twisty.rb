#############################################################################
#                                                                           #
#   Copyright (c) 2017, Ryan McCoskrie <work@ryanmccoskrie.me>              #
#                                                                           #
#   This software is distributed under the MIT License which can be found   #
#   in the root directory.                                                  #
#                                                                           #
#############################################################################

require_relative 'twisty/error.rb'
require_relative 'twisty/engine.rb'
require_relative 'twisty/command.rb'
require_relative 'twisty/entity.rb'
require_relative 'twisty/room.rb'
require_relative 'twisty/item.rb'


module Twisty

Engine.instance.define_command(
	/^help$/,
	"help",
	"Write out the list of commands you are reading now") do |x|
		engine.commands.values.each do |command|
			puts "#{command.help1}:"
			puts "#{command.help2}"
			puts
		end
	end
Engine.instance.define_command(
	/^look$/,
	"look",
	"Look at the current room") {|x| engine.current_room.look()}

Engine.instance.define_command(
	/^look at \w+$/,
	"look at object",
	"Look at a specific object") do |tokens|
		found = false
		engine.current_items.each do |id|
			if engine.items[id].name.downcase == tokens[2]
				engine.items[id].look
				found = true
				break
			end
		end

		#Error condidtion
		raise PlayError.new "I see no #{tokens[2]}" if not found
	end
Engine.instance.define_command(
	/^inventory$/,
	"inventory",
	"Get a list of things you are carrying") do |x|
		if engine.inventory.size == 0
			puts "You are currently carrying nothing"
		else
			puts "You are currently carrying: "
			engine.inventory.each{|id| puts engine.items[id].name}
		end
	end
Engine.instance.define_command(
	/^take \w+$/,
	"take object",
	"Take an object and carry it in your inventory") do |tokens|
		engine.take_item(tokens[1])
	end
Engine.instance.define_command(
	/^take \w+ from \w+$/,
	"take object from container",
	"Take an object out of a container and carry it your inventory") do |tokens|
		engine.take_from(tokens[1], tokens[3])
	end
Engine.instance.define_command(
	/^drop \w+$/,
	"drop object",
	"Remove an object from your inventory and leave it in the room") do |tokens|
		engine.drop_item(tokens[1])
	end
Engine.instance.define_command(
	/^put \w+ in \w+$/,
	"put item in container",
	"Remove an object from your inventory and leave it in a container") do |tokens|
		engine.put_item(tokens[1], tokens[3])
	end
Engine.instance.define_command(
	/^walk \w+$/,
	"walk exit",
	"Walk out of the room through an exit") do |tokens|
		found = false
		engine.current_doors.each_pair do |to, name|
			if name.downcase == tokens[1]
				engine.goto to
				found = true
				break
			end
		end

		#Error condition
		raise PlayError.new "Where is #{tokens[1]}?" unless found
	end
Engine.instance.define_command(
	/^quit$/,
	"quit",
	"Stop playing the game") {|x| engine.end_loop()}
end
