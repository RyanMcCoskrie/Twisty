Gem::Specification.new do |s|
	s.name        =                 'twisty'
	s.version     =                  '0.2.1'
	s.date        =             '2018-04-17'
	s.summary     =                 "Twisty"
	s.description = "Adventure game library"
	s.authors     =         "Ryan McCoskrie"
	s.email       =  'work@ryanmccoskrie.me'
	s.files       = [        "lib/twisty.rb",
			  "lib/twisty/engine.rb",
			 "lib/twisty/command.rb",
			   "lib/twisty/error.rb",
			  "lib/twisty/entity.rb",
			    "lib/twisty/item.rb",
			    "lib/twisty/room.rb" ]

	s.homepage    =
	 'http://gitlab.com/RyanMcCoskrie/Twisty'
	s.license     =			    'MIT'

	s.add_runtime_dependency   'rtype-native',
					'~>0.6.8'
end
